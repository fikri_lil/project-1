<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = 'buku';

    protected $fillable = ['judul', 'tahun', 'sinopsis', 'poster','genre_id','penulis_id'];
}
