<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Buku;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buku = Buku::all();

        return view('buku.index', compact('buku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = DB::table('genre')->get();
        return view('buku.create', compact ('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'judul' => 'required',
            'sinopsis' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'genre_id' => 'required',
        ]);

        $PosteName = time().'.'.$request->poster->extension();

        $request->poster->move(public_path('photo'), $PosteName);

        $buku = new Buku;
        
        $buku->judul = $request->judul;
        $buku->sinopsis = $request->sinopsis;
        $buku->tahun = $request->tahun;
        $buku->poster = $PosteName;
        $buku->genre_id = $request->genre_id;
    
        $buku->save();

        return redirect('/buku/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $buku = Buku::findOrFail($id);

        return view('buku.show', compact('buku'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $genre = DB::table('genre')->get();
        $buku = Buku::findOrFail($id);

        return view('buku.edit', compact('buku', 'genre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'sinopsis' => 'required',
            'tahun' => 'required',
            'poster' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'genre_id' => 'required',
        ]);

        $buku = Buku::find($id);

        if($request->has('poster')){                
            $PosteName = time().'.'.$request->poster->extension();

            $request->poster->move(public_path('photo'), $PosteName);

            $buku = new Buku;
            
            $buku->judul = $request->judul;
            $buku->sinopsis = $request->sinopsis;
            $buku->tahun = $request->tahun;
            $buku->poster = $PosteName;
            $buku->genre_id = $request->genre_id;
        }

        else{
            $buku->judul = $request->judul;
            $buku->sinopsis = $request->sinopsis;
            $buku->tahun = $request->tahun;
            $buku->genre_id = $request->genre_id;
        }
        
        $flight->save();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
