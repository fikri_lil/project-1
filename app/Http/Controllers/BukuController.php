<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;

class bukuController extends Controller
{
    public function create(){
        return view('buku.create');
    }
    public function store(Request $request){
        $request->validate([
            'judul' => 'required',
            'tahun' => 'required',
            'sinopsis' => 'required',
            'poster' => 'required',
        ]);
        $buku = new buku; 
     
        $buku->judul = $request->judul;
        $buku->tahun = $request->tahun;
        $buku->sinopsis = $request->sinopsis;
        $buku->poster = $request->poster;
     
        $buku->save();
    
        return redirect('/buku');
       }
    
       public function index()
       {
           $buku = buku::all();
            return view('buku.index', compact('buku'));    
       }
    
       public function show($buku_id)
       {
           $buku = buku::where('id', $buku_id)->first();
           return view('buku.show', compact('buku'));
       }
    
      
}
