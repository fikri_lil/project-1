<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Penulis;

class PenulisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penulis = Penulis::all();

        return view('penulis.index', compact('penulis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penulis.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required|max:2',
                'alamat' => 'required',
            ],
            [
                'nama.required' => 'Silahkan isi Nama Aktor terlebih dahulu',
                'umur.required' => 'Silahkan isi Umur terlebih dahulu',
                'umur.max' => 'Silahkan isi Umur dengan benar',
                'alamat.required' => 'Silahkan isi Alamat terlebih dahulu',
            ]
        );
        $penulis = new Penulis;

        $penulis->nama = $request->nama;
        $penulis->umur = $request->umur;
        $penulis->alamat = $request->alamat;
        
        $penulis->save();

        return redirect('/penulis');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penulis = Penulis::find($id)->first();

        return view('penulis.show', compact('penulis'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penulis = Penulis::find($id);

        return view('penulis.edit', compact('penulis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required|max:2',
                'alamat' => 'required',
            ],
            [
                'nama.required' => 'Silahkan isi Nama Aktor terlebih dahulu',
                'umur.required' => 'Silahkan isi Umur terlebih dahulu',
                'umur.max' => 'Silahkan isi Umur dengan benar',
                'alamat.required' => 'Silahkan isi Alamat terlebih dahulu',
            ]
        );
        $penulis = Penulis::find($id);

        $penulis->nama = $request->nama;
        $penulis->umur = $request->umur;
        $penulis->alamat = $request->alamat;
        
        $penulis->save();

        return redirect('/penulis');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penulis = Penulis::find($id);
        $penulis->delete();
        return redirect('/penulis');
    }
}
