<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\genre;

class genreController extends Controller
{
    public function create(){
        return view('genre.create');
    }
 
    public function store(Request $request){
     $request->validate([
         'nama' => 'required',
     ]);
     $genre = new genre; 
  
     $genre->nama = $request->nama;
     $genre->save();
 
     return redirect('/genre');
    }
    public function index()
   {
       $genre = genre::all();
        return view('genre.index', compact('genre'));    
   }

   public function show($genre_id)
   {
       $genre = genre::where('id', $genre_id)->first();
       return view('genre.show', compact('genre'));
   }

   public function edit($genre_id)
   {
    $genre = genre::where('id', $genre_id)->first();
    return view('genre.edit', compact('genre'));
   }

   public function update(Request $request, $genre_id)
   {
    $request->validate([
        'nama' => 'required',
    ]);

    $genre = genre::find($genre_id);
 
    $genre->nama = $request['nama'];
 
    $genre->save();
    return redirect('/genre');
   }
   public function destroy($genre_id)
   {
    $genre = genre::find($genre_id);
 
    $genre->delete();
    return redirect('/genre');
   }
}
     

