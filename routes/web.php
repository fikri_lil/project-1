<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BerandaController@index');


// CRUD Penulis
// CREAT
Route::get('/penulis/create', 'PenulisController@create');
Route::post('/penulis', 'PenulisController@store');
// READ
Route::get('/penulis', 'PenulisController@index');
Route::get('/penulis/{penulis_id}', 'PenulisController@show');
// UPDATE
Route::get('/penulis/{penulis_id}/edit', 'PenulisController@edit');
Route::put('/penulis/{penulis_id}', 'PenulisController@update');
// DELETE
Route::delete('/penulis/{penulis_id}', 'PenulisController@destroy');

// CRUD genre
// CREAT
Route::get('/genre/create', 'genreController@create'); //Route Menambahkan film baru
 Route::post('/genre', 'genreController@store');
// READ
Route::get('/genre', 'genreController@index'); //Route List Cast
 Route::get('/genre/{genre_id}', 'genreController@show');
 //update
 
 Route::get('/genre/{genre_id}/edit', 'genreController@edit'); //Route Menuju Form Edit
 Route::put('/genre/{genre_id}', 'genreController@update'); //Route Untuk update data di database

// Delete
Route::delete('/genre/{genre_id}', 'genreController@destroy');



// CRUD Buku
Route::resource('/buku', 'BukuController');

Auth::routes();

