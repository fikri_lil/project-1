<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{asset('template/index3.html')}}" class="brand-link">
      <img src="{{asset('template/dist/img/AdminLTELogo.png')}}" style="opacity: .8" alt="AdminLTE Logo" class="brand-image img-circle elevation-3">
      <span class="brand-text font-weight-light">AdminLTE 3</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('template/dist/img/raffiprofil.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="https://gitlab.com/Raffi1412" class="d-block">Raffi Pratama</a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>Home</p>
            </a>
          </li>
          @auth
            <li class="nav-item">
                <a href="/genre" class="nav-link">
                  <i class="nav-icon fas fa-folder"></i>
                  <p>Daftar Genre</p>
                </a>
              </li>
          @endauth
          <li class="nav-item">
            <a href="/buku/" class="nav-link">
              <i class="nav-icon fas fa-play-circle"></i>
              <p>Daftar Buku</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fas fa-table"></i>
              <p>
                Tables
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/data-tables" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Table</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/penulis" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Penulis</p>
                </a>
              </li>
            </ul>
          </li>
          {{-- Logout --}}
          @auth
          <li class="nav-item bg-danger" aria-labelledby="navbarDropdown">
            <a class="nav-link" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();">
                <i class="nav-icon fas fa-sign-out-alt"></i>
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
        @endauth
        @guest
          <li class="nav-item bg-info">
                <a href="/login" class="nav-link">
                  <i class="nav-icon fas fa-edit"></i>
                  <p>Login</p>
                </a>
              </li>
        @endguest

        {{-- Penutup Logout --}}
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>