@extends('layout.master')
    
@section('title')
    Detail Penulis
@endsection

@section('content')

    <h1>{{ $penulis->nama }}</h1>
    <h3>{{ $penulis->umur }}</h3>
    <p>{{ $penulis->alamat }}</p>

@endsection