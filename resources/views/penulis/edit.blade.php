@extends('layout.master')
    
@section('title')
    Halaman Edit Penulis
@endsection

@section('content')

    <form method="POST" action="/penulis/{{ $penulis->id }}">
    @csrf
    @method('PUT')


    <div class="form-group">
            <label>Nama Penulis</label>
            <input type="text" name="nama" value="{{ $penulis->nama }}" class="form-control">
    </div>
    @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <div class="form-group">
            <label>Umur</label>
            <input type="number" name="umur" value="{{ $penulis->umur }}" class="form-control">
    </div>
    @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
    @enderror


    <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" cols="30" rows="10" class="form-control"> {{ $penulis->alamat }} </textarea>
    </div>
    @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-secondary btn-sm">Save</button>
</form>

@endsection