@extends('layout.master')
    
@section('title')
    Halaman Pendaftaran Penulis
@endsection

@section('content')

<form method="POST" action="/penulis">
    @csrf
    <div class="form-group">
            <label>Nama Penulis</label>
            <input type="text" name="nama" class="form-control">
    </div>

    @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
            <label>Umur</label>
            <input type="number" name="umur" class="form-control">
    </div>

    @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
    @enderror

    <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" cols="30" rows="10" class="form-control"></textarea>
    </div>

    @error('alamat')
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-secondary">Create</button>
</form>

@endsection
