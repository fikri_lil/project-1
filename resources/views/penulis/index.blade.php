@extends('layout.master')

@section('title')
    List Data Penulis
@endsection

@section('content')

<a href="/penulis/create" class="btn btn-secondary mb-3">Tambah Penulis</a>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Alamat</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($penulis as $key => $item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->umur }}</td>
            <td>{{ $item->alamat }}</td>
            <td>
                <a href="/penulis/{{$item -> id}}" class="mt-2 btn btn-info btn-sm">Info</a>
                <a href="/penulis/{{$item -> id}}/edit" class="mt-2 btn btn-dark btn-sm">Edit</a>

                <form class="mt-2" action="/penulis/{{$item -> id}}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
    @empty
        <h1>Data Tidak Ada</h1>
    @endforelse
  </tbody>
</table>

@endsection