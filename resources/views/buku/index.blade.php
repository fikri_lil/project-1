@extends('layout.master')
    
@section('title')
    List Daftar Buku
@endsection

@section('content')

<div class="row">
    @forelse($buku as $item)
        <div class="col-4">
            <div class="card">
                <img src="{{asset('photo/'. $item->poster)}}" class="card-img-top" alt="">
                <div class="card-body">
                    <h3>{{$item->judul}}</h3>
                    <h4>{{$item->tahun}}</h4>
                    <p class="card-text"> {{Str::limit($item->sinopsis, 30)}} </p>
                    <div class="btn-group">
                        <a href="/buku/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/buku/{{$item->id}}/edit" class="btn btn-dark btn-sm">Edit</a>
                        <a href="#" class="btn btn-danger btn-sm">Delete</a>
                    </div>
                </div>
            </div>
        </div>
    @empty
        <h4>Data Buku Belum Tersedia</h4>
    @endforelse
</div>

@endsection