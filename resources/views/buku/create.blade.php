@extends('layout.master')
    
@section('title')
    Input Data Buku
@endsection

@section('content')

<<<<<<< HEAD
<form action='/buku' method='POST' enctype="multipart/form-data">
  @csrf
  <div class="mb-3">
    <label class="form-label">Judul Buku</label>
    <input type="text" name="judul" class="form-control">
  </div>
  @error('judul')
  <div class="alert alert-danger">{{ $message }} </div>
  @enderror
  <div class="mb-3">
    <label class="form-label">Tahun Terbit</label>
    <input type="number" name="tahun" class="form-control">
  </div>
  @error('tahun')
  <div class="alert alert-danger">{{ $message }} </div>
  @enderror
  <div class="mb-3">
    <label >Sinopsis Buku</label>
    <textarea name="sinopsis" class="form-control"></textarea>
  </div>
  @error('sinopsis')
  <div class="alert alert-danger">{{ $message }} </div>
  @enderror
  <div class="mb-3">
    <label class="form-label">Poster Buku</label>
    <input type="text" name="poster" class="form-control">
  </div>
  @error('poster')
  <div class="alert alert-danger">{{ $message }} </div>
  @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
=======
<form method="POST" action="/genre" encntype="multipart/form-data">
        @csrf
        <div class="form-group">
                <label>Judul Buku</label>
                <input type="text" name="judul" class="form-control">
        </div>
        @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
>>>>>>> d814a413c0a3e5fc9016163d315cd98235d092d9

        <div class="form-group">
                <label>Tahun Terbit</label>
                <input type="number" name="tahun" class="form-control">
        </div>
        @error('tahun')
                <div class="alert alert-danger">{{$message}}</div>
        @enderror
        
        <div class="form-group">
                <label>Sinopsis</label>
                <textarea name="sinopsis" cols="30" rows="10" class="form-control"></textarea>
        </div>
        @error('poster')
                <div class="alert alert-danger">{{ $message }}</div>
        @enderror


        <div class="form-group">
                <label>Poster</label>
                <input type="file" name="poster" class="form-control">
        </div>
        @error('poster')
                <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group">
                <label>Genre</label>
                <select name="genre_id" class="form-control" id="">
                <option value="">---Pilih Genre---</option>
                @foreach($genre as $item)
                        <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
                </select>
        </div>
        @error('genre_id')
                <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <button type="submit" class="btn btn-secondary">Create</button>
</form>

@endsection