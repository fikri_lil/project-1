@extends('layout.master')
    
@section('title')
    Edit Data Buku
@endsection

@section('content')

<form method="POST" action="/genre{{$genre->id}}" encntype="multipart/form-data">
    @csrf
    <div class="form-group">
            <label>Judul Buku</label>
            <input type="text" value="{{$buku->judul}}" name="judul" class="form-control">
    </div>
    @error('judul')
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <div class="form-group">
            <label>Sinopsis</label>
            <textarea name="sinopsis" cols="30" rows="10" class="form-control">{{$buku->sinopsis}}</textarea>
    </div>
    @error('sinopsis')
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <div class="form-group">
            <label>Tahun</label>
            <input type="number" value="{{$buku->tahun}}" name="tahun" class="form-control">
    </div>
    @error('tahun')
            <div class="alert alert-danger">{{$message}}</div>
    @enderror


    <div class="form-group">
            <label>Poster</label>
            <input type="file" name="poster" class="form-control">
    </div>
    @error('poster')
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <div class="form-group">
        <label>Genre</label>
        <select name="genre_id" class="form-control" id="">
            <option value="">---Pilih Genre---</option>
            @foreach($genre as $item)
                @if($item->id === $buku->genre_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
            @endforeach
        </select>
    </div>
    @error('genre_id')
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <button type="submit" class="btn btn-secondary">Save</button>
</form>

@endsection