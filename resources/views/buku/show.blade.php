@extends('layout.master')

@section('judul')
Detail Buku
@endsection

@section('content')

<img class="mr-3" src="{{asset('gambar/'.$buku->poster)}}" alt="Generic placeholder image">
<h1>{{$buku->judul}}</h1>
<h3>{{$buku->tahun}}</h3>
<p>{{$buku->sinopsis}}</p>

<a href="/buku" class="btn btn-secondary">Kembali</a>

@endsection