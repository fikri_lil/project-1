@extends('layout.master')

@section('title')
    Beranda
@endsection

@section('content')

<h1 class= "font-weight-bolder text-center">WELCOME TO MY LIBRARY</h1>
    <h2 class="text-center">Selamat datang di Perpustakaan Digital MY LIBRARY</h2>
    <h3 class="text-center">"Jendela Digital untuk melihat dunia"</h3>
    <p class="text-center">Buka jendela digitalmu <a href="/login">disini</a> </h3>
@endsection