@extends('layout.master')

@section('judul')
Tambah Pemain
@endsection

@section('content')

<form action = '/genre' method='POST'>
  @csrf
  <div class="mb-3">
    <label class="form-label">Nama Genre</label>
    <input type="text" name="nama" class="form-control">
  </div>
  @error('nama')
  <div class="alert alert-danger">{{ $message }} </div>
  @enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>


@endsection