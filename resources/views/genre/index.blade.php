@extends('layout.master')

@section('title')
    List Data Genre
@endsection

@section('content')

<a href="/genre/create" class="btn btn-secondary mb-3">Tambah Genre</a>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Nama</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($genre as $key => $item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>
                <a href="/genre/{{$item -> id}}" class="mt-2 btn btn-info btn-sm">Info</a>
                <a href="/genre/{{$item -> id}}/edit" class="mt-2 btn btn-dark btn-sm">Edit</a>

                <form class="mt-2" action="/genre/{{$item -> id}}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
    @empty
        <h1>Data Tidak Ada</h1>
    @endforelse
  </tbody>
</table>

@endsection