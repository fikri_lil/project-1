@extends('layout.master')
    
@section('title')
    Input Data Genre
@endsection

@section('content')

<form method="POST" action="/genre">
    @csrf
    <div class="form-group">
            <label>Nama Genre</label>
            <input type="text" name="nama" class="form-control">
    </div>

    @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-secondary">Create</button>
</form>

@endsection