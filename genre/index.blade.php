@extends('layout.master')

@section('title')
    List Genre
@endsection

@section('content')

<a href="/genre/create" class="btn btn-secondary mb-3">Tambah Genre</a>

<table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No.</th>
      <th scope="col">Nama Genre</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($genre as $key => $item)
        <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ $item->nama }}</td>
            <td>
                <form action="/genre/{{$item -> id}}" method="POST">
                  @csrf
                  @method('delete')
                  <input type="submit" value="Delete" class="btn btn-danger btn-sm btn-sm">
                </form>
            </td>
        </tr>
    @empty
        <h1>Data Tidak Ada</h1>
    @endforelse
  </tbody>
</table>

@endsection